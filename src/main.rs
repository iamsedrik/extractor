#[macro_use]
extern crate structopt;
extern crate exitcode;
extern crate walkdir;

use std::fs::copy;
use std::path::PathBuf;
use std::process::exit;
use std::process::Command;
use structopt::StructOpt;
use walkdir::WalkDir;

#[derive(StructOpt, Debug)]
#[structopt(name = "extractor")]
struct Opt {
    input: String,
    output: String,
}

fn validate_path(path: &PathBuf) -> bool {
    if path.is_file() || path.is_dir() {
        true
    } else {
        println!("{:?} is not a file or a directory", path);
        exit(exitcode::DATAERR);
    }
}

fn handle_file(input: &PathBuf, output: &mut PathBuf) {
    output.push(input.file_name().expect("a file"));
    copy(&input, &output).unwrap();
}

fn main() {
    let opt = Opt::from_args();
    let input = PathBuf::from(opt.input);
    let mut output = PathBuf::from(opt.output);

    validate_path(&input);
    validate_path(&output);

    if input.is_file() {
        handle_file(&input, &mut output);
    } else {
        for entry in WalkDir::new(&input).into_iter().filter_map(|e| e.ok()) {
            let extension = match entry.path().extension() {
                None => continue,
                Some(os_str) => os_str.to_str().unwrap(),
            };
            match extension.to_lowercase().as_ref() {
                "rar" => {
                    Command::new("unrar")
                        .args(&[
                            "x",
                            entry.path().to_str().unwrap(),
                            output.to_str().unwrap(),
                        ]).output()
                        .expect("to run the unrar command successfully");
                    ()
                }
                "mkv" | "mp4" => {
                    handle_file(&entry.path().to_owned(), &mut output);
                    ()
                }
                _ => continue,
            };
        }
    }
}
